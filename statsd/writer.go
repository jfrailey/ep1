package stats

//Convenience Library to allow a 1 liner for all statsd metric gathering

import (
	"log"
	"sync"

	"github.com/quipo/statsd"
)

var statsdclient *statsd.StatsdClient
var statsMutex = &sync.Mutex{}

//create a statsd connection
func CreateStatsConnection(address string, prefix string) error {

	defer statsMutex.Unlock()
	statsMutex.Lock()

	statsdclient = statsd.NewStatsdClient(address, prefix)
	err := statsdclient.CreateSocket()
	if nil != err {
		log.Println(err)
		return err
	}
	return nil
}

// IncrementStat - convenient method to just add to any func to instrument
func IncrementStat(metric string, cnt int64) error {
	err := statsdclient.Incr(metric, cnt)
	if err != nil {
		// put in resilient recovery checking here. If socket dies...
		return err
	}
	return nil
}
