# README #

Working with a top ranking software company in the RTP area, this REPO provides an interchange for 
artifacts on code sniplets, technical discussions, and the ultimate code repo for some program utilities.

### What is this repository for? ###

* Foobar application. 
* 1.0
* Created 10/15/2017

### How do I get set up? Checkout-Build-Run ###

* Clone this repo for fast start
```
### make a new directory, cd to it, then git clone repo
$ mkdir jfrailey 
$ cd jfrailey 
$ git clone https://jfrailey@bitbucket.org/jfrailey/ep1.git src/bitbucket.org/ep1  

## export GOPATH, GOBIN
$ export GOPATH=$PWD 
$ export GOBIN=$GOPATH/bin

## get dependencies and build binary
$ go get ./...
$ go install ./... 

## IMPORTANT NOTE: 
## DEFAULT PORT IS SET TO "80" 
## IF YOU RUN UNDER PORT 1024, THIS SCRIPT MUST BE RAN UNDER ROOT SESSION 
## FROM HERE ON DOWN !!! 
##
## if you want to run HTTP on alternate port, set this ENV variable
$ export HTTP_PORT_OVERRIDE=8080 //or any port

## if you want to send statsd to alternate from default of localhost, set this ENV var
$ export STATSD_ADDRESS_OVERRIDE="10.10.10.8:8125" //must contain address and port

## run health checking script, which will start up server if not running
$ python -u src/bitbucket.org/ep1/foobar.py > log.txt 2>&1 &

## Send a HTTP query for both, Question 1 and Question 2 using curl
$ curl localhost:8080/foobar/healthstatus //returns status code

## echo's back json, as well as sends metric via statsD to Graphite
$ curl localhost:8080?count=1 -d '{"Geo": {"CityName": "NewYorkCity", "ContinentCode": "NA", "CountryCode":"US"}}'

## health check script will keep running. You can kill the server and the healthscript 
## by sending a kill signal to the foobar PID. It will send a kill to the server.
$ pkill -f foobar.py
```

NOTE: This code was tested on OSX and Ubuntu16.04 linux

## Question 1 ##

How did i answer this q1 ? 

I saw a need for a few features in a web server that seemed simple enough. I wrote a simple Webserver in GO. Seemed like the emphasis was with the program and script interaction.

#### GO Server ####
* Two endpoints  
```
  "/foobar/healthcheck" = answer status 200 to all request it receives`
  "/" = this is used for question 2 below
```

* writes a q1.pid file in it's root to tell another instance, as well as a script, a service is running
* error out if an existing q1.pid file already exists
* accept a graceful shutdown on a SIGNAL or <cntrl-c> , finish any existing calls in progress, and remove the q1.pid

#### Monitor Script(s) ####
```
#### foobar.py ####
```
From the instructions, to create (2) functions in the script

* Identify that there is a PID running (if the PID does not exist, start the server)   
If it receives (5) network errors in a row, it does a kill on the PID and restarts the service.  
Approx 1 hr to write the script and refactor for multiple questions.  

* If the running instance is confirmed, then send a request and wait for a 200 OK status return on /foobar/healthcheck. If this fails, restart server  
If it receives (5) 500 errors in a row, it sends a KILLHUP to the PID, if that kills it, everything shuts down a bit cleaner.  
Approx 1 hr to integrate the kill signal handler into the script  

My motiviation of for the python, was to not impose any python dependencies to be downloaded via "pip" or otherwise for this simple exercise. Python has amazing libraries available, and the use of these are simple enough to automate, and enhance much richer calls for scripting. Not knowing where the target is for this to run, seems always a simpler bet to stay minimalistic.  

In hindsight, I would have done a 'pip install psutil', and used this library to have made a more elegant and reliable solution.  

## Question 2 ##
This seemed a bit more straightforward. I did however have to do some homework on statsD and took some time to install the Vagrant image of synthesis. I have to admit i did wander a bit, learning some cool new features on this setup. I have some experience with InfluxDB products, and have to say i do like them a bit better. 

* approx 4+ hrs discovering all features of Graphite setup, and how to feed, graph data
* approx 1+ hr working on the new golang library for statsD to use inside the webserver
* approx 1-2 hrs writing webserver code and graceful shutdown

## Question 3 ##
#### microService and Redunancy
I would stage all the new service installs, before i shutdown the old ones. If i controlled the software, I would assure the old servers would be able to catch a messages (network or kill etc) and would stop accepting new socket connections, but continue to service existing connections. I would send that signal, immediate after bring up the new ones, so that all new connections occurred directly after the old servers received kill signal. It would be my assuption there would be nanosecond delays, and network retries would not drop any live packets.

Redunancy implies load balancing. There are (2) considerations one must make to choose the correct load balancing design.  
* Stateless client request: preferred if viable from application. This can assure no rules implied that client request has to rely on state or 'session' and allows faster/fewer server side constraints, on matching up client context with server saved state.  
* Stateful client request: this will required either a sticky session bit, to assure client is redirected back to original server for saved client session, or replicating client session state among a server cluster. This requires more server side overhead and complexity, but several tools and techniques are available to achieve this.


#### DNS Option  
I would look into the ability to keep all the DNS records offline on the new installs during setup. I believe you could also just setup the nodes, and have them ready but not enabled in the ELB service. I would write a script to concurrently shutdown all old services, and lock the URL until the new ones were online.

## Question 4 
I have some experience with InfluxDB TSDB. I would try to leverage telegraph agents to instrument all running instances and microservices either in containers, or on host. Providing built in collectionD like features, would give me a large selection of common system metrics and I would inject statsD libcalls on all my microservice instances. The use of a global configuration n/v servers are also more common and can provide excellent centralized lookup, and configuration management. (/etcd, consul, etc)

InfluxDB provides ability to inline trigger events based on thresholds one can set, on inbound metrics. These events could be wired to self healing and notification events to best proactivey fix or alert anything that goes out of self healing capabilities. These events would be best wired to agents and CLI libraries that could bring PODS up/down, or better based on the microservice details and options. The Chronograph service allows a very rapidly customizable Dashboard that can be shared on any network browser also, for live and historic analytics and queries one could form trends and patterns with.

* john@hupla.com
I have (2) quotes that I will share, that mean a lot to me. 

> Perfection is achieved, not when there is nothing more to add, but when there is nothing left to take away. Antoine de Saint-Exupery
