"""
healthCheck.py: Monitors health of webserver
                bounces if the PID file is not present
                bounces if the PID is present but won't get a socket connect to web port
                bounces if connection to web port does not return a 200
"""
import httplib
import socket, sys
import os
import subprocess
import time
import signal
import sys

# import http.client
__author__      = "John Frailey - Alias: dFense"
__copyright__   = "Copyright 2017, EP-Enterprise"

httpEndpoint = "/foobar/healthcheck"

## look for env port override, else use default
## if using default, then this script must be ran from root shell
httpHost = "localhost"
httpPort = "80"

if "HTTP_PORT_OVERRIDE" in os.environ:
    httpPort = os.environ['HTTP_PORT_OVERRIDE']

## Sentinel for main service control loop
keepRunning = True

def signal_handler(signal, frame):
    global keepRunning
    ## exit shutdown below
    keepRunning = False

## read the PID file from webserver. needed if you 
## have to kill the process (lib psutil is awesome but probably requires pip install)
def getpid():
    try:
        file = open("q1.pid", "r")
        pid = file.readline().rstrip('\n')
        return int(pid)
    except IOError:
        return -1

    return -2
    
        

## return int status code from http URL
## left a few print statements in used for debugging
def getstatuscode(server, port, url):
    try: 
        conn = httplib.HTTPConnection(httpHost + ":" + port)
        conn.request("GET", httpEndpoint)
        r1 = conn.getresponse()
    except socket.gaierror, exception:
       # print "=> Caught [%s]" % exception.__class__.__name__ 
       return -2
    except Exception as exception:
        print "=> Failed with exception [%s]" % exception.__class__.__name__
        return -3

    # print "=> %d %s" % (r1.status, r1.reason)
    return r1.status

def restartserver(): 
    ## TODO add timestamp to log messages
    print "=> restarting server"
    stopserver()
    startserver()

def startserver():
    print "=> starting server"
    process = subprocess.Popen(["bin/q2exec", "&"])
    time.sleep(.5) 

def stopserver(): 
    ## TODO could check ps | grep "q2exec"
    ## killall assures if one was in half baked state, restart and get clean PID file
    print "=> killing server"
    process = subprocess.Popen(["killall", "q2exec", ">" , "/dev/null", "2>&1"])
    # process = subprocess.Popen(["killall", "q2exec", ">", "/dev/null", "2>&1"])
    time.sleep(.5) 

## Entry point for monitor script
if __name__ == "__main__":

    ## track number of failures
    statusErrors = 0
    cnt = 0

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)


    ## start service loop here. kill signal will shut the service and it down
    while keepRunning: 
        result = getpid()
        if result == -1:
            restartserver()
        ## print "=> StatusCode: %d" % getstatuscode("localhost", httpPort, httpEndpoint)
        result =  getstatuscode("localhost", httpPort, httpEndpoint)
        if result != 200:
            print "=> GestStatusCode Error: %d" % (result)
            statusErrors += 1
            if statusErrors >=5:
                print "restart service"
                restartserver()
        else: 
            statusErrors = 0
        
        time.sleep(5)

    stopserver()


