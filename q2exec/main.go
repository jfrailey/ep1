package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/ep1/q2"
)

// Entry point in for both WebServers in Exercise
// Just specify which entry in the CLI
func main() {

	q2 := q2.WebServer{}
	q2.StartSimpleWebserver()

	// signal handling
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		fmt.Println()
		fmt.Println(sig)
		done <- true
	}()
	<-done

	q2.StopSimpleWebServer()

}
