package q2

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	stats "bitbucket.org/ep1/statsd"
)

// constants that can be overridden on command line or in config if desired
const (
	pidFile = "q1.pid"
	metric  = "foobar"
	prefix  = "ep2."
)

var hserver *http.Server
var statsdAddress = "localhost:8125"
var httpPort = ":80"

// WebServer - holds the configuration and state of WebServer
type WebServer struct {
	port int
}

// Message - struct to match url body
type GeoInstance struct {
	City          string `json:"CityName"`
	ContinentCode string
	Iso           string `json:"CountryIsoCode"`
}

type Message struct {
	Geo GeoInstance
}

// curl localhost:8000 -d '{"Geo":{"CityName":"NewYorkCity","ContinentCode":"NA","CountryIsoCode":"US"}}'
func defaultRoot(w http.ResponseWriter, r *http.Request) {

	count := r.URL.Query().Get("count")
	//validate proper number conversion
	if count == "" {
		http.Error(w, "count param was not provided in query string", 400)
		return
	}

	stats.IncrementStat("ep2", 1)

	// Read body
	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// hold the json POST body
	var msg Message
	err = json.Unmarshal(b, &msg)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	//just echo back result
	output, err := json.Marshal(msg)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.Write(output)
}

// very simple endpoint, that returns a 200 if it's up and running
func healthcheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("☄ HTTP status code returned!"))
}

// StartSimpleWebserver - entry point for webserver to begin.
// went instance based for ability to have caller keep a
// reference for shutdown
// StartSimpleWebserver - very simple webserver to just test status code returns
func (s *WebServer) StartSimpleWebserver() {

	writePID() //create a simple pid file on file system
	statsOverride := os.Getenv("STATSD_ADDRESS_OVERRIDE")
	if statsOverride != "" {
		statsdAddress = statsOverride
	}
	stats.CreateStatsConnection(statsdAddress, prefix)
	mux := http.NewServeMux() //allows us to create a srv and store reference to it

	//healthcheck endpoint
	mux.Handle("/foobar/healthcheck", http.HandlerFunc(healthcheck))
	//send them to target entry point
	mux.Handle("/", http.HandlerFunc(defaultRoot))
	httpPortOverride := os.Getenv("HTTP_PORT_OVERRIDE")
	if httpPortOverride != "" {
		httpPort = ":" + httpPortOverride
	}
	log.Printf("Starting Web Server on port: %s\n", httpPort)
	hserver = &http.Server{Addr: httpPort, Handler: mux}
	go func() {
		// service connections
		if err := hserver.ListenAndServe(); err != nil {
			log.Printf("listen: %s\n", err)
		}
	}()
}

// writePID - put the PID of this process into
// a text file, that a script can read. Keeping cross platform by putting in root of app
// if a pid file already exist, just abort, meaning catastrophic error on previous shutdown
// such as someone pulling the power plug
func writePID() {

	_, err := os.Stat(pidFile)
	if !os.IsNotExist(err) {
		log.Fatal("Existing PID file exists. Server already running or error shutting down previous instance")
	}

	file, err := os.Create(pidFile)
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()
	fmt.Fprintf(file, "%s", strconv.Itoa(os.Getpid()))
}

// deletePID - remove the PID file
func deletePID() {
	os.Remove(pidFile)
}

// StopSimpleWebServer - graceful shutdown and allows removal of pid file
func (s WebServer) StopSimpleWebServer() {
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	hserver.Shutdown(ctx)
	deletePID()
}
